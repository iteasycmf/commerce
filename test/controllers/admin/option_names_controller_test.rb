require 'test_helper'

class Admin::OptionNamesControllerTest < ActionController::TestCase
  setup do
    @option_name = option_names(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:option_names)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create option_name" do
    assert_difference('OptionName.count') do
      post :create, option_name: { active: @option_name.active, name: @option_name.name, position: @option_name.position, type: @option_name.type, value: @option_name.value }
    end

    assert_redirected_to admin_option_name_path(assigns(:option_name))
  end

  test "should show option_name" do
    get :show, id: @option_name
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @option_name
    assert_response :success
  end

  test "should update option_name" do
    patch :update, id: @option_name, option_name: { active: @option_name.active, name: @option_name.name, position: @option_name.position, type: @option_name.type, value: @option_name.value }
    assert_redirected_to admin_option_name_path(assigns(:option_name))
  end

  test "should destroy option_name" do
    assert_difference('OptionName.count', -1) do
      delete :destroy, id: @option_name
    end

    assert_redirected_to admin_option_names_path
  end
end
