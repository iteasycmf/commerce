class ActionController::Base
  #before_action :set_carter

  def set_carter
    if current_user.nil?
      if session[:cart_id]
        begin
          @cart = Cart.find(session[:cart_id])
          rescue ActiveRecord::RecordNotFound
            @rtcart=RecordType.find_by_machine_name(:cart)
            @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new)
            session[:cart_id] = @cart.id
        end
      else
        @rtcart=RecordType.find_by_machine_name(:cart)
        @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new)
        session[:cart_id] = @cart.id
      end
    else
      if session[:cart_id]
        begin
        @cart = Cart.find(session[:cart_id])
        if @cart.user_id.nil?
          @cart.update(user_id: current_user.id)
        end
        rescue ActiveRecord::RecordNotFound
        @rtcart=RecordType.find_by_machine_name(:cart)
        @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new)
        session[:cart_id] = @cart.id
      end
      else
        @rtcart=RecordType.find_by_machine_name(:cart)
        @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new, user_id: current_user.id)
        session[:cart_id] = @cart.id
      end
    end
  end

end