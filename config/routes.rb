Rails.application.routes.draw do
  namespace :admin do
    resources :option_names
  end
  namespace :admin do
    resources :fci_skus
    resources :fci_button_buys
    resources :fci_quantities
    resources :fci_prices
    resources :carts
    resources :orders
    resources :line_items
  end
  resources :line_items
  resources :orders
  resources :carts
  get 'line_items/:id/cart_plus' => 'line_items#cart_plus', as: 'cart_plus'
  get 'line_items/:id/cart_minus' => 'line_items#cart_minus', as: 'cart_minus'
end
