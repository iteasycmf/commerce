class Admin::OptionNamesController < ApplicationController
  before_action :set_option_name, only: [:show, :edit, :update, :destroy]

  # GET /admin/option_names
  def index
    @option_names = OptionName.all
  end

  # GET /admin/option_names/1
  def show
  end

  # GET /admin/option_names/new
  def new
    @option_name = OptionName.new
  end

  # GET /admin/option_names/1/edit
  def edit
  end

  # POST /admin/option_names
  def create
    @option_name = OptionName.new(option_name_params)

    if @option_name.save
      redirect_to [:admin, @option_name], notice: 'Option name was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/option_names/1
  def update
    if @option_name.update(option_name_params)
      redirect_to [:admin, @option_name], notice: 'Option name was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/option_names/1
  def destroy
    @option_name.destroy
    redirect_to admin_option_names_url, notice: 'Option name was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_option_name
      @option_name = OptionName.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def option_name_params
      params.require(:option_name).permit(:name, :type, :value, :active, :position)
    end
end
