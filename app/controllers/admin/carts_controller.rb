class Admin::CartsController < Admin::BaseController
  before_action :set_cart, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  layout 'admin'
  # GET /admin/carts
  def index
    @carts = Cart.all
  end

  # GET /admin/carts/1
  def show
  end

  # GET /admin/carts/new
  def new
    @cart = Cart.new
  end

  # GET /admin/carts/1/edit
  def edit
  end

  # POST /admin/carts
  def create
    @cart = Cart.new(cart_params)

    if @cart.save
      redirect_to [:admin, @cart], notice: 'Cart was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/carts/1
  def update
    if @cart.update(cart_params)
      instance_saver(@cart, params)
      redirect_to [:admin, @cart], notice: 'Cart was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/carts/1
  def destroy
    @cart.destroy
    redirect_to admin_carts_url, notice: 'Cart was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cart
    @cart = Cart.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def cart_params
    params.require(:cart).permit(:status, :record_type_id, :user_id)
  end
end
