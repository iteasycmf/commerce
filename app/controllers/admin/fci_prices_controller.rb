class Admin::FciPricesController < Admin::BaseController
  before_action :set_fci_price, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  layout 'admin'
  # GET /admin/fci_prices
  def index
    @fci_prices = FciPrice.all
  end

  # GET /admin/fci_prices/1
  def show
  end

  # GET /admin/fci_prices/new
  def new
    @fci_price = FciPrice.new
  end

  # GET /admin/fci_prices/1/edit
  def edit
  end

  # POST /admin/fci_prices
  def create
    @fci_price = FciPrice.new(fci_price_params)

    if @fci_price.save
      redirect_to [:admin, @fci_price], notice: 'Fci price was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/fci_prices/1
  def update
    if @fci_price.update(fci_price_params)
      redirect_to [:admin, @fci_price], notice: 'Fci price was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/fci_prices/1
  def destroy
    @fci_price.destroy
    redirect_to admin_fci_prices_url, notice: 'Fci price was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_price
    @fci_price = FciPrice.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def fci_price_params
    params.require(:fci_price).permit(:value, :currency, :buy, :name_buy, :fci_id, :mid, :model)
  end
end
