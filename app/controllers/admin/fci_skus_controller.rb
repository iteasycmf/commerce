class Admin::FciSkusController < ApplicationController
  before_action :set_fci_sku, only: [:show, :edit, :update, :destroy]

  # GET /admin/fci_skus
  def index
    @fci_skus = FciSku.all
  end

  # GET /admin/fci_skus/1
  def show
  end

  # GET /admin/fci_skus/new
  def new
    @fci_sku = FciSku.new
  end

  # GET /admin/fci_skus/1/edit
  def edit
  end

  # POST /admin/fci_skus
  def create
    @fci_sku = FciSku.new(fci_sku_params)

    if @fci_sku.save
      redirect_to [:admin, @fci_sku], notice: 'Fci sku was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/fci_skus/1
  def update
    if @fci_sku.update(fci_sku_params)
      redirect_to [:admin, @fci_sku], notice: 'Fci sku was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/fci_skus/1
  def destroy
    @fci_sku.destroy
    redirect_to admin_fci_skus_url, notice: 'Fci sku was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_sku
    @fci_sku = FciSku.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def fci_sku_params
    params.require(:fci_sku).permit(:value, :override)
  end
end
