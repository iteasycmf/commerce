class Admin::LineItemsController < Admin::BaseController
  before_action :set_line_item, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  layout 'admin'
  # GET /admin/line_items
  def index
    @line_items = LineItem.all
  end

  # GET /admin/line_items/1
  def show
  end

  # GET /admin/line_items/new
  def new
    @line_item = LineItem.new
  end

  # GET /admin/line_items/1/edit
  def edit
  end

  # POST /admin/line_items
  def create
    @line_item = LineItem.new(line_item_params)

    if @line_item.save
      redirect_to [:admin, @line_item], notice: 'Line item was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/line_items/1
  def update
    if @line_item.update(line_item_params)
      instance_saver(@line_item, params)
      redirect_to [:admin, @line_item], notice: 'Line item was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/line_items/1
  def destroy
    @line_item.destroy
    redirect_to admin_line_items_url, notice: 'Line item was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_line_item
    @line_item = LineItem.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def line_item_params
    params.require(:line_item).permit(:total_price, :total_quantity, :status, :record_type_id, :mid, :model, :order_id, :cart_id, :oneprice)
  end
end
