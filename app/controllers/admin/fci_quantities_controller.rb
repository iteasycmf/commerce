class Admin::FciQuantitiesController < Admin::BaseController
  before_action :set_fci_quantity, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  layout 'admin'
  # GET /admin/fci_quantities
  def index
    @fci_quantities = FciQuantity.all
  end

  # GET /admin/fci_quantities/1
  def show
  end

  # GET /admin/fci_quantities/new
  def new
    @fci_quantity = FciQuantity.new
  end

  # GET /admin/fci_quantities/1/edit
  def edit
  end

  # POST /admin/fci_quantities
  def create
    @fci_quantity = FciQuantity.new(fci_quantity_params)

    if @fci_quantity.save
      redirect_to [:admin, @fci_quantity], notice: 'Fci quantity was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/fci_quantities/1
  def update
    if @fci_quantity.update(fci_quantity_params)
      redirect_to [:admin, @fci_quantity], notice: 'Fci quantity was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/fci_quantities/1
  def destroy
    @fci_quantity.destroy
    redirect_to admin_fci_quantities_url, notice: 'Fci quantity was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_quantity
    @fci_quantity = FciQuantity.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def fci_quantity_params
    params.require(:fci_quantity).permit(:value, :measuring, :active, :fci_id, :mid, :model)
  end
end
