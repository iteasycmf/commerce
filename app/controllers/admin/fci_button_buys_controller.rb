class Admin::FciButtonBuysController < Admin::BaseController
  before_action :set_fci_button_buy, only: [:show, :edit, :update, :destroy]

  # GET /admin/fci_button_buys
  def index
    @fci_button_buys = FciButtonBuy.all
  end

  # GET /admin/fci_button_buys/1
  def show
  end

  # GET /admin/fci_button_buys/new
  def new
    @fci_button_buy = FciButtonBuy.new
  end

  # GET /admin/fci_button_buys/1/edit
  def edit
  end

  # POST /admin/fci_button_buys
  def create
    @fci_button_buy = FciButtonBuy.new(fci_button_buy_params)

    if @fci_button_buy.save
      redirect_to [:admin, @fci_button_buy], notice: 'Fci button buy was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/fci_button_buys/1
  def update
    if @fci_button_buy.update(fci_button_buy_params)
      redirect_to [:admin, @fci_button_buy], notice: 'Fci button buy was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/fci_button_buys/1
  def destroy
    @fci_button_buy.destroy
    redirect_to admin_fci_button_buys_url, notice: 'Fci button buy was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fci_button_buy
    @fci_button_buy = FciButtonBuy.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def fci_button_buy_params
    params.require(:fci_button_buy).permit(:text_buy, :fci_id, :mid, :model, :type)
  end
end
