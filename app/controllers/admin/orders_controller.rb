class Admin::OrdersController < Admin::BaseController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  load_and_authorize_resource
  # GET /admin/orders
  def index
    @q = Order.search(params[:q])
    @orders = @q.result.page params[:page]
  end

  # GET /admin/orders/1
  def show
  end

  # GET /admin/orders/new
  def new
    @order = Order.new
  end

  # GET /admin/orders/1/edit
  def edit
  end

  # POST /admin/orders
  def create
    @order = Order.new(order_params)

    if @order.save
      redirect_to [:admin, @order], notice: 'Order was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/orders/1
  def update
    if @order.update(order_params)
      instance_saver(@order, params)
      redirect_to [:admin, @order], notice: 'Order was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/orders/1
  def destroy
    @order.destroy
    redirect_to admin_orders_url, notice: 'Order was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def order_params
    params.require(:order).permit(:total_price, :total_quantity, :status, :record_type_id, :user_id)
  end
end
