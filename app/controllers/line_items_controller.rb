class LineItemsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :set_line_item, only: [:show, :edit, :update, :destroy, :cart_plus, :cart_minus]

  # GET /line_items
  def index
    @line_items = LineItem.all
  end

  def cart_plus
    qua = @line_item.total_quantity += 1
    @line_item.update(total_quantity: qua)
    redirect_to :back, notice: 'Вы обновили корзину!'
  end

  def cart_minus
    qua = @line_item.total_quantity -= 1
    @line_item.update(total_quantity: qua)
    redirect_to :back, notice: 'Вы обновили корзину!'
  end

  # GET /line_items/1
  def show
  end

  # GET /line_items/new
  def new
    @line_item = LineItem.new
  end

  # GET /line_items/1/edit
  def edit
  end

  # POST /line_items
  def create
    if session[:cart_id]
      @cart = Cart.find_by(id: session[:cart_id], status: :new)
      if @cart.nil?
        if current_user.nil?
          @rtcart=RecordType.find_by_machine_name(:cart)
          @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new)
          session[:cart_id] = @cart.id
        else
          @cart = Cart.find_by(user_id: current_user.id, status: :new)
          if @cart.nil?
            @rtcart=RecordType.find_by_machine_name(:cart)
            @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new, user_id: current_user.id)
          end
          session[:cart_id] = @cart.id
        end
      end
    else
      if current_user.nil?
        @rtcart=RecordType.find_by_machine_name(:cart)
        @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new)
        session[:cart_id] = @cart.id
      else
        @cart = Cart.find_by(user_id: current_user.id, status: :new)
        if @cart.nil?
          @rtcart=RecordType.find_by_machine_name(:cart)
          @cart = Cart.create(record_type_id: @rtcart.id, entity: :cart, status: :new, user_id: current_user.id)
        end
        session[:cart_id] = @cart.id
      end
    end
    if params[:items].present?
      params[:items].each do |item|
        unless item[:total_quantity] == 0
          current_line = LineItem.find_by(cart_id: @cart.id, record_type_id: params[:line_item][:record_type_id], mid: item[:mid], model: item[:model])
          if current_line
            qua = current_line.total_quantity + item[:total_quantity].to_i
            current_line.update(total_quantity: qua)
          else
            @line_item = LineItem.new(cart_id: @cart.id, record_type_id: params[:line_item][:record_type_id], mid: item[:mid], model: item[:model], total_quantity: item[:total_quantity].to_i)
            instance_saver(@line_item, params)
            @line_item.update(cart_id: session[:cart_id].to_i)
          end
        end
      end
    else
      current_line = LineItem.find_by(cart_id: @cart.id, record_type_id: params[:line_item][:record_type_id], mid: params[:line_item][:mid], model: params[:line_item][:model])
      if current_line
        qua = current_line.total_quantity + params[:line_item][:total_quantity].to_i
        current_line.update(total_quantity: qua)
      else
        @line_item = LineItem.new(line_item_params)
        instance_saver(@line_item, params)
        @line_item.update(cart_id: session[:cart_id].to_i)
      end
    end
    redirect_to :back, notice: 'Вы добавили товар в корзину!'
  end

  # PATCH/PUT /line_items/1
  def update
    if @line_item.update(line_item_params)
      instance_saver(@line_item, params)

      redirect_to @line_item, notice: 'Line item was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /line_items/1
  def destroy
    @line_item.destroy
    redirect_to :back, :gflash => {:success => 'Вы удалили товар из заказа'}
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_line_item
    @line_item = LineItem.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def line_item_params
    params.require(:line_item).permit(:total_price, :total_quantity, :status, :record_type_id, :mid, :model, :order_id, :cart_id, :oneprice)
  end
end
