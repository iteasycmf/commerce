class FciPrice < ApplicationRecord
  belongs_to :fci
  before_save :zero_price

  def zero_price
    if self.value.nil?
      self.value == 0
    end
  end
end
