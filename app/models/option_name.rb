class OptionName < ApplicationRecord
  has_many :option_values
  has_many :option_relatives
end
