class Cart < ApplicationRecord
  belongs_to :record_type
  belongs_to :user
  has_many :model_relations, -> { where model_main: 'Cart' }, :foreign_key => 'mid_main', dependent: :destroy
  has_many :fcis, through: :record_type
  has_many :line_items
  has_many :orders
  after_save :set_record_type
  after_save :set_entity
  #after_save :in_order
  after_save :calc_skidka
  default_scope { order("created_at DESC") }

  def set_record_type
    if self.record_type_id.nil?
      self.record_type_id=RecordType.find_by_machine_name(:cart).id
    end
  end

  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def in_order
    if status_changed?
      if status == 'ping'
        order = Order.create(status: 'ping', user_id: user_id, cart_id: id)
        line_items.map{|ln| ln.update(order_id: order.id)}
      end
    end
  end

  def calc_skidka
    if status == 'new' and !user_id.nil? and !line_items.empty?
      client = Savon::Client.new(wsdl: 'http://149.202.222.162:80/base3s_admin_c/ws/WebSite.1cws?wsdl', basic_auth: ["callcenter", "Rfrf<zrf"])
      kodc = user.profile.kodcs.take.value
      nomerd = user.profile.nomer_dogovoras.take.value
      p = line_items.map{|line| "{'UIDClient': '', 'CodeClient': '#{kodc}' 'NameClient': '', 'UIDDogovor': '', 'CodeDogovor': '#{nomerd}', 'NameDogovor': '', 'UIDTovar': '', 'CodeTovar': '#{line.product.kods.take.value}', 'NameTovar': '', 'quantity': '#{line.total_quantity}', 'Price': '#{line.total_price}', 'SostoyanieZakaza': '', 'NumberOfOrderOnSite': '', 'isDropshipping': false, 'PriceDropshipping': 0, 'UIDAdress': '', 'NameAdress': '','CodeAdress': '', 'TypeOfDelivery': ''}"}
      pj = p.to_s.gsub('"', '')
      response = client.call(:order_price, message: {"JSON_goods_order"=>"{'#value': "+pj+"}", "NumberOfOrderOnSite"=>''})
      comone = response.body[:order_price_response][:return]
      parsed = JSON.parse(comone)["#value"]
      parsed.each do |prod|
        kodp = prod['CodeTovar']
        line_items.map{|e| @kodp = e if e.product.kods.take.value.to_s == kodp}
        totalp = prod['Price']*@kodp.total_quantity
        currency = @kodp.product.tsenas.take.currency
        case currency
          when 'USD'
            @uatotal = totalp*26.40
          when 'EUR'
            @uatotal = totalp*29.55
          when 'UAH'
            @uatotal = totalp
          when 'CZK'
            @uatotal = totalp*27.04
          when 'RUR'
            @uatotal = totalp*0.46
          else
            @uatotal = totalp
        end

        @kodp.update(total_price: @uatotal, oneprice: prod['Price'])

      end

    end
  end

end
