class Order < ApplicationRecord
  belongs_to :record_type
  has_many :model_relations, -> { where model_main: 'Order' }, :foreign_key => 'mid_main', dependent: :destroy
  has_many :fcis, through: :record_type
  has_many :line_items
  belongs_to :cart
  belongs_to :user
  before_save :set_record_type
  before_save :set_entity
  before_save :translit
  after_create :car
  after_save :calc_skidka

  paginates_per 30
  default_scope {order("created_at DESC")}

  def calc_skidka
    if status == 'ping' and !user_id.nil? and !cart.line_items.empty?
        client = Savon::Client.new(wsdl: 'http://149.202.222.162:80/base3s_admin_c/ws/WebSite.1cws?wsdl', basic_auth: ["callcenter", "Rfrf<zrf"])
        kodc = user.profile.kodcs.take.value
        nomerd = user.profile.nomer_dogovoras.take.value
        p = cart.line_items.map{|line| "{'UIDClient': '', 'CodeClient': '#{kodc}' 'NameClient': '', 'UIDDogovor': '', 'CodeDogovor': '#{nomerd}', 'NameDogovor': '', 'UIDTovar': '', 'CodeTovar': '#{line.product.kods.take.value}', 'NameTovar': '', 'quantity': '#{line.total_quantity}', 'Price': '#{line.total_price}', 'SostoyanieZakaza': '', 'NumberOfOrderOnSite': '', 'isDropshipping': 'false', 'PriceDropshipping': '0', 'UIDAdress': '', 'NameAdress': '','CodeAdress': '', 'TypeOfDelivery': ''}"}
        pj = p.to_s.gsub('"', '')
        response = client.call(:order_price, message: {"JSON_goods_order"=>"{'#value': "+pj+"}", "NumberOfOrderOnSite"=>"#{self.id}"})
    end
  end

  def car
    cart.update(status: 'close')
  end


  def set_record_type
    if self.record_type_id.nil?
      self.record_type_id=RecordType.find_by_machine_name(:order).id
    end
  end

  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def status_name
    case status
      when 'new'
        'Новый'
      when 'ping'
        'Ожидает подтверждения'
      else
        'Не определен'
    end
  end

  def translit
    if self.record_type.has_title?
      if self.url.blank?
        @fci= Fci.find_by(name: self.record_type.label_title, mid: self.record_type.id)
        unless @fci.nil?
          @value= FciString.find_by(fci_id: @fci.id, mid: self.id)
          unless @value.nil?
            @translit = Russian.translit(@value.value)
            self.url = @translit.parameterize("_")
          end
        end
      end
    end
  end
end
