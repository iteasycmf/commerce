class LineItem < ApplicationRecord
  belongs_to :order
  belongs_to :cart
  belongs_to :record_type
  has_many :model_relations, -> { where model_main: 'LineItem' }, :foreign_key => 'mid_main', dependent: :destroy
  has_many :fcis, through: :record_type
  belongs_to :tovar, :class_name => "Cell", :foreign_key => 'mid'

  default_scope { order("created_at DESC") }
  before_create :set_record_type
  before_create :set_entity
  before_create :set_status
  before_save :set_price

  def set_record_type
    if self.record_type_id.nil?
      self.record_type_id=RecordType.find_by_machine_name(:line_item).id
    end
  end

  def set_entity
    unless self.record_type_id.nil?
      if self.entity.nil?
        self.entity = self.record_type.machine_name
      end
    end
  end

  def set_status
    if self.status.nil?
      self.status='new'
    end
  end


  def set_price
    unless self.mid and self.model and self.total_quantity == nil
      fieldc_price = FieldCustom.find_by_machine_name(:fci_prices)
      product = self.model.constantize.find(self.mid)
      if product.fcis.find_by(field_custom_id: fieldc_price.id).nil?
        role_price = false
        product.cells.each(public: true) do |cell|
          cell_price = cell.fcis.find_by(field_custom_id: fieldc_price.id)
          unless cell_price.nil?
            role_field = FciInteger.find_by(model: 'Cell', mid: cell.id)
            if User.current.nil?
              product.cells.each(public: true) do |cell|
                cell_price = cell.fcis.find_by(field_custom_id: fieldc_price.id)
                unless cell_price.nil?
                  role_field = FciInteger.find_by(model: 'Cell', mid: cell.id)
                  if role_field.value == 3
                    field_price = FciPrice.find_by(model: 'Cell', mid: cell.id)
                    self.oneprice= field_price.value if self.oneprice == 0
                    total_price = self.total_quantity * self.oneprice
                    currency = self.product.tsenas.take.currency
                    case currency
                      when 'USD'
                        @uatotal = total_price*26.40
                      when 'EUR'
                        @uatotal = total_price*29.55
                      when 'UAH'
                        @uatotal = total_price
                      when 'CZK'
                        @uatotal = total_price*27.04
                      when 'RUR'
                        @uatotal = total_price*0.46
                      else
                        @uatotal = total_price
                    end
                    self.total_price = @uatotal
                  end
                end
              end
            else
              User.current.roles.each do |role|
                if role_field.value == role.id
                  role_price = true
                  field_price = FciPrice.find_by(model: 'Cell', mid: cell.id)
                  self.oneprice= field_price.value if self.oneprice == 0
                  total_price = self.total_quantity * self.oneprice
                  currency = self.product.tsenas.take.currency
                  case currency
                    when 'USD'
                      @uatotal = total_price*26.40
                    when 'EUR'
                      @uatotal = total_price*29.55
                    when 'UAH'
                      @uatotal = total_price
                    when 'CZK'
                      @uatotal = total_price*27.04
                    when 'RUR'
                      @uatotal = total_price*0.46
                    else
                      @uatotal = total_price
                  end
                  self.total_price = @uatotal
                end
              end
            end
          end
        end
        if role_price == false
          product.cells.each(public: true) do |cell|
            cell_price = cell.fcis.find_by(field_custom_id: fieldc_price.id)
            unless cell_price.nil?
              role_field = FciInteger.find_by(model: 'Cell', mid: cell.id)
              if role_field.value == 3
                field_price = FciPrice.find_by(model: 'Cell', mid: cell.id)
                self.oneprice= field_price.value if self.oneprice == 0
                total_price = self.total_quantity * self.oneprice
                currency = self.product.tsenas.take.currency
                case currency
                  when 'USD'
                    @uatotal = total_price*26.40
                  when 'EUR'
                    @uatotal = total_price*29.55
                  when 'UAH'
                    @uatotal = total_price
                  when 'CZK'
                    @uatotal = total_price*27.04
                  when 'RUR'
                    @uatotal = total_price*0.46
                  else
                    @uatotal = total_price
                end
                self.total_price = @uatotal
              end
            end
          end
        end
      else
        field_price = FciPrice.find_by(model: self.model, mid: self.mid)
        self.oneprice= field_price.value if self.oneprice == 0
        total_price = self.total_quantity * self.oneprice
        currency = self.product.tsenas.take.currency
        case currency
          when 'USD'
            @uatotal = total_price*26.40
          when 'EUR'
            @uatotal = total_price*29.55
          when 'UAH'
            @uatotal = total_price
          when 'CZK'
            @uatotal = total_price*27.04
          when 'RUR'
            @uatotal = total_price*0.46
          else
            @uatotal = total_price
        end
        self.total_price = @uatotal
      end
    end
  end

  def product
    model.constantize.find(mid)
  end

end
