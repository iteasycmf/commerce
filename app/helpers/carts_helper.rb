module CartsHelper

  def current_cart
    Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      session.delete(:cart_id)
  end

  def current_cart_price(format)
    unless current_cart.nil?
      summa = current_cart.line_items.sum("total_price")
      case format
        when 'currency'
          case default_currency
            when 'USD'
              '$'+summa.to_s
            when 'UAH'
              summa.to_s+' грн.'
          end
        when 'nil'
          summa
      end
    else
      0.00
    end
  end

  def current_cart_qua
    unless current_cart.nil?
      current_cart.line_items.sum("total_quantity")
    else
      0.00
    end
  end

end
