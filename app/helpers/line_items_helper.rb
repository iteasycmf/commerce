module LineItemsHelper
  def line_item_new
    LineItem.new
  end

  def line_item_price(item,format)
      summa = item.total_price
      case format
        when 'currency'
          case default_currency
            when 'USD'
              '$'+summa.to_s
            when 'UAH'
              summa.to_s+' грн.'
          end
        when 'nil'
          summa
      end
  end


  def line_item_product(item)
    item.model.constantize.find(item.mid.to_i)
  end

end
