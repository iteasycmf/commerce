module PricesHelper

  def price_full(model)

  end

  def available_currency
    System.find_by_key(:available_currency).value.split("|")
  end

  def default_currency
    System.find_by_key(:default_currency).value
  end

  def priceuah(val, format)
    case format
      when 'USD'
          val.to_d*26.40
        when 'EUR'
          val.to_d*29.55
        when 'UAH'
          val
        when 'CZK'
          val.to_d*27.04
        when 'RUR'
          val.to_d*0.46
        else
          val
    end
  end

  def price(val, format)
    case format
      when 'currency'
        case default_currency
          when 'USD'
            '$'+val.to_s
          when 'UAH'
            val.to_s+' грн.'
        end
      when 'nil'
        val
    end
  end
end
