class CreateFciQuantities < ActiveRecord::Migration
  def change
    create_table :fci_quantities do |t|
      t.decimal :value
      t.string :measuring
      t.boolean :active
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
