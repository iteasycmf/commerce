class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.string :status
      t.references :record_type, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :entity

      t.timestamps null: false
    end
  end
end
