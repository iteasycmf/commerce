class CreateOptionRelatives < ActiveRecord::Migration[5.0]
  def change
    create_table :option_relatives do |t|
      t.references :option_name, foreign_key: true
      t.integer :position
      t.boolean :require
      t.boolean :active
      t.integer :mid
      t.string :model

      t.timestamps
    end
  end
end
