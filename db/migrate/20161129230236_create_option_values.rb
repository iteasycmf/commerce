class CreateOptionValues < ActiveRecord::Migration[5.0]
  def change
    create_table :option_values do |t|
      t.string :value
      t.decimal :quantity
      t.string :type_price
      t.decimal :price
      t.boolean :percent
      t.string :sku
      t.boolean :default
      t.references :option_name, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps
    end
  end
end
