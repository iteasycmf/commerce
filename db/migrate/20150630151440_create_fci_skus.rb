class CreateFciSkus < ActiveRecord::Migration
  def change
    create_table :fci_skus do |t|
      t.string :value
      t.boolean :override
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
