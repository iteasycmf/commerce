class CreateFciPrices < ActiveRecord::Migration
  def change
    create_table :fci_prices do |t|
      t.decimal :value
      t.string :currency
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model

      t.timestamps null: false
    end
  end
end
