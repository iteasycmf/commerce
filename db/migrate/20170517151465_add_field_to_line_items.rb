class AddFieldToLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :line_items, :oneprice, :decimal, :default => 0
  end
end