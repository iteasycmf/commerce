class CreateFciButtonBuys < ActiveRecord::Migration
  def change
    create_table :fci_button_buys do |t|
      t.string :text_buy
      t.references :fci, index: true, foreign_key: true
      t.integer :mid
      t.string :model
      t.string :type
      t.boolean :active

      t.timestamps null: false
    end
  end
end
