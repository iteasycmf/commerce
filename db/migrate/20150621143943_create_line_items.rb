class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.decimal :total_price
      t.decimal :total_quantity
      t.string :status
      t.references :record_type, index: true, foreign_key: true
      t.integer :mid
      t.string :model
      t.references :order, index: true, foreign_key: true
      t.references :cart, index: true, foreign_key: true
      t.string :entity

      t.timestamps null: false
    end
  end
end
