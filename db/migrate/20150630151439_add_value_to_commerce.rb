class AddValueToCommerce < ActiveRecord::Migration
  def up
    System.create([{key: 'lineitem', name: 'Line Item', machine_category: :record_type, value: 'LineItem', blocked: true, hidden: true},
                   {key: 'Order', name: 'Order', machine_category: :record_type, value: 'Order', blocked: true, hidden: true},
                   {key: 'cart', name: 'Cart', machine_category: :record_type, value: 'Cart', blocked: true, hidden: true},
                   {key: 'default_currency', name: 'Default Currency', machine_category: :commerce_price, value: 'USD', blocked: true, hidden: false},
                   {key: 'available_currency', name: 'Available Currency', machine_category: :commerce_price, value: 'USD|UAH|EUR', blocked: true, hidden: false}])
    FieldCustom.create([{name: :price, machine_name: :fci_prices, active: true, value: '{"render":"admin/fci_prices/form_field",
 "model":"FciPrice", "param":[{"line":"value", "value":"fci_price_value"}, {"line":"currency", "value":"fci_price_currency"}],
 "renderfront":"shared/fields/fci_price"}'},
                        {name: :quantity, machine_name: :fci_quantities, active: true, value: '{"render":"admin/fci_quantities/form_field",
 "model":"FciQuantity", "param":[{"line":"value", "value":"fci_quantity_value"}, {"line":"measuring", "value":"fci_quantity_measuring"},
{"line":"active", "value":"fci_quantity_active"}], "renderfront":"shared/fields/fci_quantity"}'},
                        {name: :button_buy, machine_name: :fci_button_buys, active: true, value: '{"render":"admin/fci_button_buys/form_field",
 "model":"FciButtonBuy", "param":[{"line":"text_buy", "value":"fci_button_buy_text_buy"}, {"line":"type", "value":"fci_button_buy_type"},
 {"line":"active", "value":"fci_button_buy_active"}], "renderfront":"shared/fields/fci_button_buy"}'},
                        {name: :sku, machine_name: :fci_skus, active: true, value: '{"render":"admin/fci_skus/form_field",
 "model":"FciSku", "param":[{"line":"value", "value":"fci_sku_value"}, {"line":"override", "value":"fci_sku_override"}],
"renderfront":"shared/fields/fci_sku"}'}
                       ])
    MenuItem.create([{name: 'Orders', url: 'admin/orders', menu_id: 1, public: true, position: 50, csscls: 'icon-basket-loaded'},
                     {name: 'LineItems', url: 'admin/line_items', menu_id: 1, public: true, position: 51, csscls: 'icon-book-open'}])

    %w( Cart Order LineItem ).each do |subject|
      %w( create read update destroy ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: true, name: action+" to "+subject)
      end
    end

    %w( Cart Order LineItem ).each do |subject|
      %w( create read update destroy ).each do |action|
        Permission.create(subject_class: subject, action: action, admin: false, name: action+" to "+subject)
      end
    end

    RecordType.create([{name: 'Default order', desc: 'Adding standard orders',
                        url: :order, disabled: false, machine_name: :order, blocked: true, model: 'Order'},
                       {name: 'Default cart', desc: 'Adding standard carts',
                        url: :cart, disabled: false, machine_name: :cart, blocked: true, model: 'Cart'},
                       {name: 'Default LineItems', desc: 'Adding standard line_items',
                        url: :line_item, disabled: false, machine_name: :line_item, blocked: true, model: 'LineItem'}])


  end
end
