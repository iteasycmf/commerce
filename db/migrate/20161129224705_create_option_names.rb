class CreateOptionNames < ActiveRecord::Migration[5.0]
  def change
    create_table :option_names do |t|
      t.string :name
      t.string :type
      t.text :value
      t.boolean :active
      t.integer :position

      t.timestamps
    end
  end
end
