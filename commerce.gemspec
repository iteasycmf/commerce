$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "commerce/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "commerce"
  s.version     = Commerce::VERSION
  s.authors     = ["Eugene Peluhnya"]
  s.email       = ["peluhnya@outlook.com"]
  s.homepage    = "http://iteasycode.com/"
  s.summary     = "http://iteasycode.com/"
  s.description = "http://iteasycode.com/"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]


  s.add_dependency "rails", '5.0.4'


end
